"use strict";
jQuery(document).ready(function($){
  // Window on load
  $("aside.nav li.tree-menu .icon").click(function(){
    $(this).parent().next("ul.tree").slideToggle();
    $(this).toggleClass("rotate-icon");
  });
  $(".feedlyTabsUnPin").click(function(){
    $("aside.nav").animate({width:"51px"},200).addClass("hide unpin").removeClass("pin");
    $("aside.nav > *").css("display","none");
    $('main#content').addClass('active');
  });
  $(".feedlyTabsPin").click(function(){
    $("aside.nav").removeClass("unpin").addClass("pin");
    $('main#content').removeClass('active');
  });
  $(document).mouseup(function (e) {
    if (!$("aside.nav").is(e.target) && $("aside.nav").has(e.target).length === 0 && $("aside.nav").hasClass("unpin"))
    {
      $("aside.nav").animate({width:"51px"},200).addClass("hide");;
      $("aside.nav > *").css("display","none");
      $('main#content').addClass('active');
   }else if($("aside.nav").hasClass("hide")){
    $("aside.nav").animate({width:"268"},200).removeClass("hide");
    $("aside.nav > *").css("display","block");
    $('main#content').removeClass('active');
   }
  });
  //res sidebar
  $('#icon-sidebar').click(function(){
    $('aside.nav').toggleClass('active');
    $('.mobile-menu-curtain').show();
    if(!$('aside.nav').hasClass('active')){
      $('.mobile-menu-curtain').hide();
    }
  });
  $('.mobile-menu-curtain').click(function(){
    $('aside.nav').removeClass('active');
    $('.mobile-menu-curtain').hide();
  })
  $(window).on('load', function(){
    console.log($(window).width());
    var wwid = $(window).width();
    if(wwid <= 769){
      $('aside.nav').removeClass('unpin');
    }
  });

});
